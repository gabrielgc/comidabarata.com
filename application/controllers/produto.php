<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->db->select('*');
        $this->db->where('produto_status', "1");
        $dados['produto'] = $this->db->get('produto')->result();

        $this->load->view('inc/head-adm');
        $this->load->view('inc/menu_left');
        $this->load->view('produtos/all_adverts', $dados);
        $this->load->view('inc/footer-adm');
    }

    public function my_adverts() {
        $this->db->select('*');
        $this->db->where('produto_status', "1");
        $this->db->where('produto_user_id', $this->session->userdata('user_id'));
        $dados['produto'] = $this->db->get('produto')->result();

        $this->load->view('inc/head-adm');
        $this->load->view('inc/menu_left');
        $this->load->view('produtos/my_adverts', $dados);
        $this->load->view('inc/footer-adm');
    }
    public function all_adverts() {
        $this->db->select('*');
        $this->db->where('produto_status', "1");
        $dados['produto'] = $this->db->get('produto')->result();

        $this->load->view('inc/head-adm');
        $this->load->view('inc/menu_left');
        $this->load->view('produtos/all_adverts', $dados);
        $this->load->view('inc/footer-adm');
    }

    public function registerProduct() {
        
        $this->db->select('*');
        $dados['tipo_produto'] = $this->db->get('tipo_produto')->result();
        
        
        
        $this->load->view('inc/head-adm');
        $this->load->view('inc/menu_left');
        $this->load->view('produtos/register_product',$dados);
        $this->load->view('inc/footer-adm');
    }

    public function saveRegister() {

        // recebe os dados do formulário
        $data['produto_nome'] = $this->input->post('produto_nome');
        $data['produto_preco_novo'] = $this->input->post('produto_preco_novo');
        $data['produto_user_id']= $this->session->userdata('user_id');
        $data['produto_tipo_produto_id']= $this->input->post('produto_tipo_produto');
        $data['produto_status'] = 1;

        // define as configurações para upload da foto
        $config['upload_path'] = './images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 1024;
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        $this->upload->do_upload('produto_foto');

        $arquivo = $this->upload->data();
        $data['produto_foto'] = $arquivo['file_name'];

        if ($this->db->insert('produto', $data)) {

// recarrega a view (index)
            redirect(base_url('produto'));
        } else {
            $this->load->view('inc/head-adm');
            $this->load->view('inc/menu_left');
            $this->load->view('produtos/register_product');
            $this->load->view('inc/footer-adm');
        }
    }

    public function formUpdate($produto_id = null) {
        $this->db->where('produto_id', $produto_id);
        $data['produto'] = $this->db->get('produto')->result();

        $this->load->view('inc/head-adm');
        $this->load->view('inc/menu_left');
        $this->load->view('produtos/update_product', $data);
        $this->load->view('inc/footer-adm');
    }
    
     public function formProduct($id = null) {
        $this->db->where('produto_id', $id);
        $data['produto'] = $this->db->get('produto')->result();

        $this->load->view('inc/head-adm');
        $this->load->view('inc/menu_left');
        $this->load->view('produtos/product', $data);
        $this->load->view('inc/footer-adm');
    }

    public function update() {
        // recebe os dados do formulário
        $id = $this->input->post('produto_id');

        $data['produto_nome'] = $this->input->post('produto_nome');
        $data['produto_preco_novo'] = $this->input->post('produto_preco_novo');
        $data['produto_status'] = $this->input->post('produto_status');

        $this->db->where('produto_id', $id);
        if ($this->db->update('produto', $data)) {
            // recarrega a view (index)
            redirect(base_url('produto'));
        }
    }
    
    public function comprar() {
        
        // recebe os dados do formulário
        $data['produto_id'] = $this->input->post('produto_id');
        $data['user_id'] = $_SESSION["user_id"];
        $data['produto_estoque'] = $this->input->post('produto_estoque');
        $data['produto_status'] = 1;


        if ($this->db->insert('transacao', $data)) {

// recarrega a view (index)
            redirect(base_url('produto/historicoCompras'));
        } else {
            $this->load->view('inc/head-adm');
            $this->load->view('inc/menu_left');
            $this->load->view('produtos?erro');
            $this->load->view('inc/footer-adm');
        }
    }
    
     public function historicoCompras() {
        $this->db->select('*');
        $this->db->where('status', "1");
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->join('produto','produto_id=transacao_produto_id', 'inner' );
        $dados['transacao'] = $this->db->get('transacao')->result();
        
       

        $this->load->view('inc/head-adm');
        $this->load->view('inc/menu_left');
        $this->load->view('produtos/historico', $dados);
        $this->load->view('inc/footer-adm');
    }
    
    

}
