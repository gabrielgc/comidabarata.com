

<section id="container" >
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="<?= base_url(); ?>" class="logo"><b>COMIDABARATA<span>.com</span></b></a>
        <!--logo end-->

        <div class="btn-group pull-right top-menu" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Menu
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a class="" href="<?= base_url(); ?>user">Perfil</a></li>
                    <li><a class="" href="<?= base_url(); ?>">Sair</a></li>
                </ul>
            </div>
        </div>
    </header>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-lg-11 main-chart">

                    <!--
                    ----------------------------------------------------------
                    -->

                    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->

                    <!--main content start-->
                    <section id="">
                        <section class="wrapper">
                            <div class="row mt">
                                <div class="col-lg-12">
                                    <div class="content-panel">
                                        <h4><i class="fa fa-angle-right"></i> Meus Anúncios</h4>
                                        <section id="unseen">
                                            <table class="table table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th>Imagem</th>
                                                        <th>Produto</th>
                                                        <th class="numeric text-center">Preço</th>
                                                        <th class="numeric"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><p class="centered"><a href="profile.html"><img src="assets-adm/img/milho.png" class="img-circle" width="60"></a></p></td>
                                                        <td>
                                                            <br><h5> Sabugo de Milho </h5>
                                                        </td>
                                                        <td class="numeric text-center"><br><h5>R$ 1.38</h5></td>

                                                        <td class="numeric text-center"><br><a href="#" class="btn btn-primary" role="button">Comprar</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><p class="centered"><a href="profile.html"><img src="assets-adm/img/milho.png" class="img-circle" width="60"></a></p></td>
                                                        <td>
                                                            <br><h5> Sabugo de Milho </h5>
                                                        </td>
                                                        <td class="numeric text-center"><br><h5>R$ 1.38</h5></td>

                                                        <td class="numeric text-center"><br><a href="#" class="btn btn-primary" role="button">Comprar</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><p class="centered"><a href="profile.html"><img src="assets-adm/img/milho.png" class="img-circle" width="60"></a></p></td>
                                                        <td>
                                                            <br><h5> Sabugo de Milho </h5>
                                                        </td>
                                                        <td class="numeric text-center"><br><h5>R$ 1.38</h5></td>

                                                        <td class="numeric text-center"><br><a href="#" class="btn btn-primary" role="button">Comprar</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><p class="centered"><a href="profile.html"><img src="assets-adm/img/milho.png" class="img-circle" width="60"></a></p></td>
                                                        <td>
                                                            <br><h5> Sabugo de Milho </h5>
                                                        </td>
                                                        <td class="numeric text-center"><br><h5>R$ 1.38</h5></td>

                                                        <td class="numeric text-center"><br><a href="#" class="btn btn-primary" role="button">Comprar</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><p class="centered"><a href="profile.html"><img src="assets-adm/img/milho.png" class="img-circle" width="60"></a></p></td>
                                                        <td>
                                                            <br><h5> Sabugo de Milho </h5>
                                                        </td>
                                                        <td class="numeric text-center"><br><h5>R$ 1.38</h5></td>

                                                        <td class="numeric text-center"><br><a href="#" class="btn btn-primary" role="button">Comprar</a></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </section>
                                    </div><!-- /content-panel -->
                                </div><!-- /col-lg-4 -->			
                            </div><!-- /row -->


                        </section><! --/wrapper -->
                    </section><!-- /MAIN CONTENT -->






                </div><!-- /col-lg-3 -->
            </div><! --/row -->
        </section>
    </section>

    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
        <div class="text-center">
            2017 - COMIDABARATA.com - Todos Direitos Reservados
            <a href="<?= base_url(); ?>" class="go-top">
                <i class="fa fa-angle-up"></i>
            </a>
        </div>
    </footer>
    <!--footer end-->
</section>