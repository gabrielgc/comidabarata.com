<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>COMIDABARATA.COM &mdash; Anuncie agora é grátis</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url(); ?>assets-adm/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?= base_url(); ?>assets-adm/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets-adm/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets-adm/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets-adm/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="<?= base_url(); ?>assets-adm/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets-adm/css/style-responsive.css" rel="stylesheet">

    <script src="<?= base_url(); ?>assets-adm/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>